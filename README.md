# Rennes Cycle Stations
A small web app for find a Star Cycles docking station.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.9.

It use the Angular UI library [PrimeNG](https://www.primefaces.org/primeng/#/) licensed under MIT License Copyright © 2019 PrimeTek Informatics
